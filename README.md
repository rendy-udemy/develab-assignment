# Car Park Availability

## Task Description

Dataset: https://beta.data.gov.sg/datasets/d_23f946fa557947f93a8043bbef41dd09/view
This dataset provides detailed information about each car park. This can be treated as
static and is to be loaded with a task from the CSV file provided in the link above. You
can check it into the repository and do not have to automate the downloading and
updating of this data.
Hint: The coordinates provided are in a SVY21 format. You may have to do some
conversion to a more widely used format. An option is to use a converter
(https://www.onemap.gov.sg/apidocs/apidocs/#coordinateConverters) when importing or
a similarly implemented library.

Dataset: https://api.data.gov.sg/v1/transport/carpark-availability
The API endpoint in that link provides live updates on the parking lot availability for the
car parks. Create a task to call the API and save this data in the database. Scheduling the
task is out of the scope of this project, but if manually run, the same task should be able
to update existing data with newer data from the API if run periodically.

---

## Table of Contents

- [Task Description](#task-description)
- [Table of Contents](#table-of-contents)
- [How to Start The Service](#how-to-start-the-service)
    - [Prerequisites](#prerequisites)
    - [Prepare Database Service](#prepare-database-service)
    - [Run Spring Boot Service](#run-spring-boot-service)
    - [Open API Swagger UI](#open-api-swagger-ui)
- [Recurring Functionality](#recurring-functionality)
---

## How to start the service

### Prerequisites

- JDK Installed (minimum JDK 17 required for this project)
- Docker Installed (with docker compose enabled)

### Prepare database service
```shell
docker compose up -d 
```

### Run spring boot service
```shell
./mvnw spring-boot:run -D.spring.profiles.active=dev
```

### Open API Swagger UI

[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

---


## Recurring Functionality
In order to simplify on updating availability data periodically.
I used [JobRunr](https://www.jobrunr.io/en/) library. 