package io.develab.parkingapp.availabilityservice;

import com.opencsv.CSVReaderHeaderAware;
import io.develab.parkingapp.availabilityservice.config.ApplicationConfig;
import io.develab.parkingapp.availabilityservice.model.entity.CarPark;
import io.develab.parkingapp.availabilityservice.model.enums.CarParkType;
import io.develab.parkingapp.availabilityservice.model.enums.FreeParking;
import io.develab.parkingapp.availabilityservice.model.enums.ParkingSystemType;
import io.develab.parkingapp.availabilityservice.model.enums.ShortTermParking;
import io.develab.parkingapp.availabilityservice.repository.CarParkRepository;
import io.develab.parkingapp.availabilityservice.service.AvailabilityService;
import io.develab.parkingapp.availabilityservice.util.CoordinateConverter;
import io.develab.parkingapp.availabilityservice.util.LatLonCoordinate;
import io.develab.parkingapp.availabilityservice.util.SVY21Coordinate;
import jakarta.persistence.EntityExistsException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@ConditionalOnExpression("${app.db-init: true} and !'${spring.jpa.hibernate.ddl-auto}'.equalsIgnoreCase('none')")
@Slf4j
public class InitDbRunner implements CommandLineRunner {
    private final ApplicationConfig applicationConfig;
    private final ResourceLoader resourceLoader;

    private final CarParkRepository carParkRepository;

    private final AvailabilityService availabilityService;

    @Override
    public void run(String... args) throws Exception {
        if (StringUtils.isBlank(applicationConfig.getDbInitFile())) {
            throw new NullPointerException("app.db-init-file is undefined");
        }

        if (carParkRepository.count() > 0L) {
            throw new EntityExistsException("Table is not empty. Can't proceed to initialize DB.");
        }

        Resource resource = resourceLoader.getResource(ResourceLoader.CLASSPATH_URL_PREFIX + applicationConfig.getDbInitFile());

        List<Map<String, String>> records = new ArrayList<>();

        try (CSVReaderHeaderAware reader = new CSVReaderHeaderAware(new FileReader(resource.getFile()))) {
            Map<String, String> values;

            while ((values = reader.readMap()) != null) {
                records.add(values);
            }
        }

        initDb(records);

        try {
            availabilityService.checkAvailabilityJob();
        } catch (Exception e) {
            log.error("error when executing job", e);
        }
    }

    private void initDb(List<Map<String, String>> records) {
        List<CarPark> entities = new ArrayList<>();

        for (Map<String, String> record : records) {
            CarPark entity = CarPark.builder()
                    .carParkNo(record.get(Header.CAR_PARK_NO.getValue()))
                    .address(record.get(Header.ADDRESS.getValue()))
                    .xCoord(Double.valueOf(record.get(Header.X_COORD.getValue())))
                    .yCoord(Double.valueOf(record.get(Header.Y_COORD.getValue())))
                    .carParkType(CarParkType.fromString(record.get(Header.CAR_PARK_TYPE.getValue())))
                    .parkingSystemType(ParkingSystemType.fromString(record.get(Header.TYPE_OF_PARKING_SYSTEM.getValue())))
                    .shortTermParking(ShortTermParking.fromString(record.get(Header.SHORT_TERM_PARKING.getValue())))
                    .freeParking(FreeParking.fromString(record.get(Header.FREE_PARKING.getValue())))
                    .nightParking(record.get(Header.NIGHT_PARKING.getValue()).equalsIgnoreCase("YES"))
                    .carParkDecks(Integer.parseInt(record.get(Header.CAR_PARK_DECKS.getValue())))
                    .gantryHeight(Double.valueOf(record.get(Header.GANTRY_HEIGHT.getValue())))
                    .basement(record.get(Header.CAR_PARK_BASEMENT.getValue()).equalsIgnoreCase("Y"))
                    .createdBy("SYSTEM")
                    .updatedBy("SYSTEM")
                    .build();

            LatLonCoordinate latLonCoordinate = CoordinateConverter.computeLatLon(SVY21Coordinate.builder()
                    .easting(entity.getXCoord())
                    .northing(entity.getYCoord())
                    .build());

            entity.setLatitude(latLonCoordinate.getLatitude());
            entity.setLongitude(latLonCoordinate.getLongitude());

            entities.add(entity);
        }

        if (!entities.isEmpty()) {
            carParkRepository.saveAll(entities);
        }
    }

    @Getter
    private enum Header {
        // car_park_no,address,x_coord,y_coord,car_park_type,
        // type_of_parking_system,short_term_parking,free_parking,
        // night_parking,car_park_decks,gantry_height,car_park_basement
        CAR_PARK_NO("car_park_no"),
        ADDRESS("address"),
        X_COORD("x_coord"),
        Y_COORD("y_coord"),
        CAR_PARK_TYPE("car_park_type"),
        TYPE_OF_PARKING_SYSTEM("type_of_parking_system"),
        SHORT_TERM_PARKING("short_term_parking"),
        FREE_PARKING("free_parking"),
        NIGHT_PARKING("night_parking"),
        CAR_PARK_DECKS("car_park_decks"),
        GANTRY_HEIGHT("gantry_height"),
        CAR_PARK_BASEMENT("car_park_basement");

        private final String value;

        Header(String value) {
            this.value = value;
        }

    }
}
