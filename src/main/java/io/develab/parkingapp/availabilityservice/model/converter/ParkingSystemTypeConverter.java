package io.develab.parkingapp.availabilityservice.model.converter;

import io.develab.parkingapp.availabilityservice.model.enums.ParkingSystemType;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.apache.commons.lang3.StringUtils;

@Converter
public class ParkingSystemTypeConverter implements AttributeConverter<ParkingSystemType, String> {
    @Override
    public String convertToDatabaseColumn(ParkingSystemType parkingSystemType) {
        if (parkingSystemType == null) {
            return null;
        }

        return parkingSystemType.getValue();
    }

    @Override
    public ParkingSystemType convertToEntityAttribute(String s) {
        if (StringUtils.isBlank(s)) {
            return null;
        }

        return ParkingSystemType.fromString(s);
    }
}
