package io.develab.parkingapp.availabilityservice.model.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.stream.Stream;
import lombok.Getter;

@Schema(enumAsRef = true)
@Getter
public enum ShortTermParking {
    WHOLE_DAY("WHOLE DAY"),
    NO("NO"),
    TWELVE_HOURS("7AM-7PM"),
    TILL_NIGHT("7AM-10.30PM");

    private final String value;

    ShortTermParking(String value) {
        this.value = value;
    }

    public static ShortTermParking fromString(String s) {
        return ShortTermParking.stream()
                .filter(e -> e.getValue().equals(s))
                .findFirst()
                .orElse(null);
    }

    public static Stream<ShortTermParking> stream() {
        return Stream.of(ShortTermParking.values());
    }
}
