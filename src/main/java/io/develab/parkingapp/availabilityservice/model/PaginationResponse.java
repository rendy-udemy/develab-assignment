package io.develab.parkingapp.availabilityservice.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaginationResponse<T> {
    private List<T> content;
    private int currentPage;
    private long totalElements;
    private int totalPages;

    public static <T> PaginationResponse<T> fromPage(Page<T> domainPage) {
        PaginationResponse<T> result = new PaginationResponse<>();
        result.setCurrentPage(domainPage.getPageable().getPageNumber() + 1);
        result.setTotalPages(domainPage.getTotalPages());
        result.setTotalElements(domainPage.getTotalElements());
        result.setContent(domainPage.getContent());
        return result;
    }
}
