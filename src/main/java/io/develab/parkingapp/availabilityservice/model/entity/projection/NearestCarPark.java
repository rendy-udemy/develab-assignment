package io.develab.parkingapp.availabilityservice.model.entity.projection;

public interface NearestCarPark {
    String getCarParkNo();

    String getAddress();

    double getLatitude();

    double getLongitude();

    int getTotalLots();

    int getLotsAvailable();
}
