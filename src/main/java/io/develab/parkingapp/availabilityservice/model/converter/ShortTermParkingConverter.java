package io.develab.parkingapp.availabilityservice.model.converter;

import io.develab.parkingapp.availabilityservice.model.enums.ShortTermParking;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.apache.commons.lang3.StringUtils;

@Converter
public class ShortTermParkingConverter implements AttributeConverter<ShortTermParking, String> {
    @Override
    public String convertToDatabaseColumn(ShortTermParking shortTermParking) {
        if (shortTermParking == null) {
            return null;
        }

        return shortTermParking.getValue();
    }

    @Override
    public ShortTermParking convertToEntityAttribute(String s) {
        if (StringUtils.isBlank(s)) {
            return null;
        }

        return ShortTermParking.fromString(s);
    }
}
