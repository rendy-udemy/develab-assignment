package io.develab.parkingapp.availabilityservice.model.dto.request;

import io.develab.parkingapp.availabilityservice.model.enums.CarParkType;
import io.develab.parkingapp.availabilityservice.model.enums.ParkingSystemType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class CarParkCriteria extends PaginationCriteria {
    private CarParkType carParkType;
    private ParkingSystemType parkingSystemType;
}
