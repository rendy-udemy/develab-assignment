package io.develab.parkingapp.availabilityservice.model.dto.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.Sort;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public abstract class PaginationCriteria {
    @Min(1) int currentPage = 1;
    @Min(1) @Max(50) int pageSize = 10;
    String sortAttribute;
    Sort.Direction sortDirection;
}
