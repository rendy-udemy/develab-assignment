package io.develab.parkingapp.availabilityservice.model.converter;

import io.develab.parkingapp.availabilityservice.model.enums.FreeParking;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.apache.commons.lang3.StringUtils;

@Converter
public class FreeParkingConverter implements AttributeConverter<FreeParking, String> {
    @Override
    public String convertToDatabaseColumn(FreeParking freeParking) {
        if (freeParking == null) {
            return null;
        }

        return freeParking.getValue();
    }

    @Override
    public FreeParking convertToEntityAttribute(String s) {
        if (StringUtils.isBlank(s)) {
            return null;
        }

        return FreeParking.fromString(s);
    }
}
