package io.develab.parkingapp.availabilityservice.model.mapper;

import io.develab.parkingapp.availabilityservice.model.dto.AvailabilityDto;
import io.develab.parkingapp.availabilityservice.model.entity.projection.NearestCarPark;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

@Component
public class AvailabilityMapper {
    public AvailabilityDto toDto(NearestCarPark entity) {
        System.out.println(entity);
        return AvailabilityDto.builder()
                .address(entity.getAddress())
                .latitude(entity.getLatitude())
                .longitude(entity.getLongitude())
                .totalLots(entity.getTotalLots())
                .lotsAvailable(entity.getLotsAvailable())
                .build();
    }

    public List<AvailabilityDto> toDto(List<NearestCarPark> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public Page<AvailabilityDto> toDto(Page<NearestCarPark> entities) {
        return new PageImpl<>(toDto(entities.getContent()), entities.getPageable(), entities.getTotalElements());
    }
}
