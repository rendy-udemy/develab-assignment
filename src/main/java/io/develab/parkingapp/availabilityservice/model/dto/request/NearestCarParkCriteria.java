package io.develab.parkingapp.availabilityservice.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NearestCarParkCriteria extends PaginationCriteria {
    private double latitude;
    private double longitude;
}
