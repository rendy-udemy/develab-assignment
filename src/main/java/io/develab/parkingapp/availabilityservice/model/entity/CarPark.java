package io.develab.parkingapp.availabilityservice.model.entity;

import io.develab.parkingapp.availabilityservice.model.converter.CarParkTypeConverter;
import io.develab.parkingapp.availabilityservice.model.converter.FreeParkingConverter;
import io.develab.parkingapp.availabilityservice.model.converter.ParkingSystemTypeConverter;
import io.develab.parkingapp.availabilityservice.model.converter.ShortTermParkingConverter;
import io.develab.parkingapp.availabilityservice.model.entity.support.AuditEntity;
import io.develab.parkingapp.availabilityservice.model.enums.CarParkType;
import io.develab.parkingapp.availabilityservice.model.enums.FreeParking;
import io.develab.parkingapp.availabilityservice.model.enums.ParkingSystemType;
import io.develab.parkingapp.availabilityservice.model.enums.ShortTermParking;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class CarPark extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_park_id_seq")
    private Long id;

    @Column(nullable = false, unique = true, length = 4)
    private String carParkNo;

    private String address;

    @Column(nullable = false, columnDefinition = "numeric(10,4)")
    private Double xCoord;

    @Column(nullable = false, columnDefinition = "numeric(10,4)")
    private Double yCoord;

    @Column(nullable = false, columnDefinition = "numeric(10,4)")
    private Double latitude;

    @Column(nullable = false, columnDefinition = "numeric(10,4)")
    private Double longitude;

    @Convert(converter = CarParkTypeConverter.class)
    private CarParkType carParkType;

    @Convert(converter = ParkingSystemTypeConverter.class)
    private ParkingSystemType parkingSystemType;

    @Convert(converter = ShortTermParkingConverter.class)
    private ShortTermParking shortTermParking;

    @Convert(converter = FreeParkingConverter.class)
    private FreeParking freeParking;

    private boolean nightParking;

    private int carParkDecks;

    @Column(columnDefinition = "numeric(5,2)")
    private Double gantryHeight;

    private boolean basement;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof CarPark carPark)) {
            return false;
        }

        return new EqualsBuilder().append(isNightParking(), carPark.isNightParking())
                .append(getCarParkDecks(), carPark.getCarParkDecks()).append(isBasement(), carPark.isBasement())
                .append(getId(), carPark.getId()).append(getCarParkNo(), carPark.getCarParkNo()).append(getAddress(), carPark.getAddress())
                .append(xCoord, carPark.xCoord).append(yCoord, carPark.yCoord).append(getCarParkType(), carPark.getCarParkType())
                .append(getParkingSystemType(), carPark.getParkingSystemType()).append(getShortTermParking(), carPark.getShortTermParking())
                .append(getFreeParking(), carPark.getFreeParking()).append(getGantryHeight(), carPark.getGantryHeight()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getId()).append(getCarParkNo()).append(getAddress()).append(xCoord).append(yCoord)
                .append(getCarParkType()).append(getParkingSystemType()).append(getShortTermParking()).append(getFreeParking())
                .append(isNightParking()).append(getCarParkDecks()).append(getGantryHeight()).append(isBasement()).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("carParkNo", carParkNo)
                .append("address", address)
                .append("xCoord", xCoord)
                .append("yCoord", yCoord)
                .append("carParkType", carParkType)
                .append("parkingSystemType", parkingSystemType)
                .append("shortTermParking", shortTermParking)
                .append("freeParking", freeParking)
                .append("nightParking", nightParking)
                .append("carParkDecks", carParkDecks)
                .append("gantryHeight", gantryHeight)
                .append("basement", basement)
                .toString();
    }
}
