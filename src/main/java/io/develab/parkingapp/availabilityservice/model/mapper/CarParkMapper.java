package io.develab.parkingapp.availabilityservice.model.mapper;

import io.develab.parkingapp.availabilityservice.model.dto.CarParkDto;
import io.develab.parkingapp.availabilityservice.model.entity.CarPark;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

@Component
public class CarParkMapper {
    public CarParkDto toDto(CarPark entity) {
        return CarParkDto.builder()
                .carParkNo(entity.getCarParkNo())
                .address(entity.getAddress())
                .xCoord(entity.getXCoord())
                .yCoord(entity.getYCoord())
                .latitude(entity.getLatitude())
                .longitude(entity.getLongitude())
                .carParkType(entity.getCarParkType())
                .parkingSystemType(entity.getParkingSystemType())
                .shortTermParking(entity.getShortTermParking())
                .freeParking(entity.getFreeParking())
                .nightParking(entity.isNightParking())
                .carParkDecks(entity.getCarParkDecks())
                .gantryHeight(entity.getGantryHeight())
                .basement(entity.isBasement())
                .build();
    }

    public List<CarParkDto> toDto(List<CarPark> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public Page<CarParkDto> toDto(Page<CarPark> entities) {
        return new PageImpl<>(toDto(entities.getContent()), entities.getPageable(), entities.getTotalElements());
    }
}
