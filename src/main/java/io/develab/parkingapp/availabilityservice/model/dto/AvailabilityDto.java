package io.develab.parkingapp.availabilityservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AvailabilityDto {
    private String address;
    private double latitude;
    private double longitude;
    private int totalLots;
    private int lotsAvailable;
}
