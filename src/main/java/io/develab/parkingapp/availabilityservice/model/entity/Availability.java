package io.develab.parkingapp.availabilityservice.model.entity;

import io.develab.parkingapp.availabilityservice.model.entity.support.AuditEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.time.OffsetDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Availability extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "availability_id_seq")
    private Long id;

    @Column(nullable = false, unique = true, length = 4)
    private String carParkNo;
    private int totalLots;
    private int lotsAvailable;
    private String lotType;
    private OffsetDateTime updateTimestamp;
}
