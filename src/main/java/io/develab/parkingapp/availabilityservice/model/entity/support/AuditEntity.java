package io.develab.parkingapp.availabilityservice.model.entity.support;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import java.time.OffsetDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@MappedSuperclass
public abstract class AuditEntity {
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private OffsetDateTime createdDate;

    @CreatedBy
    @Column(nullable = false, updatable = false)
    private String createdBy;

    @UpdateTimestamp
    @Column(nullable = false)
    private OffsetDateTime updatedDate;

    @LastModifiedBy
    @Column(nullable = false)
    private String updatedBy;
}
