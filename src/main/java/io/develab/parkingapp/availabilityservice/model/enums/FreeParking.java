package io.develab.parkingapp.availabilityservice.model.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.stream.Stream;
import lombok.Getter;

@Schema(enumAsRef = true)
@Getter
public enum FreeParking {
    NO("NO"),
    FROM_MORNING("SUN & PH FR 7AM-10.30PM"),
    FROM_NOON("SUN & PH FR 1PM-10.30PM");

    private final String value;

    FreeParking(String value) {
        this.value = value;
    }

    public static FreeParking fromString(String s) {
        return FreeParking.stream()
                .filter(e -> e.getValue().equals(s))
                .findFirst()
                .orElse(null);
    }

    public static Stream<FreeParking> stream() {
        return Stream.of(FreeParking.values());
    }
}
