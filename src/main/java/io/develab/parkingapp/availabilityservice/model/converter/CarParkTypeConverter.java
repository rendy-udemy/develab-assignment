package io.develab.parkingapp.availabilityservice.model.converter;

import io.develab.parkingapp.availabilityservice.model.enums.CarParkType;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.apache.commons.lang3.StringUtils;

@Converter
public class CarParkTypeConverter implements AttributeConverter<CarParkType, String> {
    @Override
    public String convertToDatabaseColumn(CarParkType carParkType) {
        if (carParkType == null) {
            return null;
        }

        return carParkType.getValue();
    }

    @Override
    public CarParkType convertToEntityAttribute(String s) {
        if (StringUtils.isBlank(s)) {
            return null;
        }

        return CarParkType.fromString(s);
    }
}
