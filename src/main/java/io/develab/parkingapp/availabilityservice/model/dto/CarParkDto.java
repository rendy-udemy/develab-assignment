package io.develab.parkingapp.availabilityservice.model.dto;

import io.develab.parkingapp.availabilityservice.model.enums.CarParkType;
import io.develab.parkingapp.availabilityservice.model.enums.FreeParking;
import io.develab.parkingapp.availabilityservice.model.enums.ParkingSystemType;
import io.develab.parkingapp.availabilityservice.model.enums.ShortTermParking;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarParkDto {
    private String carParkNo;
    private String address;
    private Double xCoord;
    private Double yCoord;
    private Double latitude;
    private Double longitude;
    private CarParkType carParkType;
    private ParkingSystemType parkingSystemType;
    private ShortTermParking shortTermParking;
    private FreeParking freeParking;
    private boolean nightParking;
    private int carParkDecks;
    private Double gantryHeight;
    private boolean basement;
}
