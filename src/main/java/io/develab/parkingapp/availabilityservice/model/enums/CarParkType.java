package io.develab.parkingapp.availabilityservice.model.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.stream.Stream;
import lombok.Getter;

@Schema(enumAsRef = true)
@Getter
public enum CarParkType {
    BASEMENT("BASEMENT CAR PARK"),
    MULTI_STOREY("MULTI-STOREY CAR PARK"),
    SURFACE("SURFACE CAR PARK"),
    COVERED("COVERED CAR PARK");

    private final String value;

    CarParkType(String value) {
        this.value = value;
    }

    public static CarParkType fromString(String s) {
        return CarParkType.stream()
                .filter(type -> type.getValue().equals(s))
                .findFirst()
                .orElse(null);
    }

    public static Stream<CarParkType> stream() {
        return Stream.of(CarParkType.values());
    }
}
