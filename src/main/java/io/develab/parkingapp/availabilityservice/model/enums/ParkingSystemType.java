package io.develab.parkingapp.availabilityservice.model.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.stream.Stream;
import lombok.Getter;

@Schema(enumAsRef = true)
@Getter
public enum ParkingSystemType {
    ELECTRONIC("ELECTRONIC PARKING"),
    COUPON("COUPON PARKING");

    private final String value;

    ParkingSystemType(String value) {
        this.value = value;
    }

    public static ParkingSystemType fromString(String s) {
        return ParkingSystemType.stream()
                .filter(type -> type.getValue().equals(s))
                .findFirst()
                .orElse(null);
    }

    public static Stream<ParkingSystemType> stream() {
        return Stream.of(ParkingSystemType.values());
    }
}
