package io.develab.parkingapp.availabilityservice.repository;

import io.develab.parkingapp.availabilityservice.model.entity.CarPark;
import io.develab.parkingapp.availabilityservice.model.entity.projection.NearestCarPark;
import io.develab.parkingapp.availabilityservice.model.enums.CarParkType;
import io.develab.parkingapp.availabilityservice.model.enums.ParkingSystemType;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkRepository extends JpaRepository<CarPark, Long> {
    Optional<CarPark> findByCarParkNoIgnoreCase(String carParkNo);

    Page<CarPark> findAllByCarParkType(CarParkType carParkType, Pageable pageable);

    Page<CarPark> findAllByParkingSystemType(ParkingSystemType parkingSystemType, Pageable pageable);

    Page<CarPark> findAllByCarParkTypeAndParkingSystemType(CarParkType carParkType, ParkingSystemType parkingSystemType, Pageable pageable);

    @Query(value = """
select cp.car_park_no as carParkNo, cp.address, cp.latitude, cp.longitude,
       avl.total_lots as totalLots, avl.lots_available as lotsAvailable
from car_park cp
join availability avl on avl.car_park_no = cp.car_park_no
where avl.lots_available > 0
order by (abs(cp.latitude - :latitude) + abs(cp.longitude - :longitude))

""",
    countQuery = """
select count(cp.id)
from car_park cp
join availability avl on avl.car_park_no = cp.car_park_no
where avl.lots_available > 0
""",
    nativeQuery = true)
    Page<NearestCarPark> findAllNearestCarParks(@Param("latitude") double latitude, @Param("longitude") double longitude, Pageable pageable);
}
