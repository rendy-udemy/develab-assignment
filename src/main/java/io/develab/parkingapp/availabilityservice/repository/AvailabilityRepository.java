package io.develab.parkingapp.availabilityservice.repository;

import io.develab.parkingapp.availabilityservice.model.entity.Availability;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AvailabilityRepository extends JpaRepository<Availability, Long> {
    Optional<Availability> findByCarParkNo(String carParkNo);
}
