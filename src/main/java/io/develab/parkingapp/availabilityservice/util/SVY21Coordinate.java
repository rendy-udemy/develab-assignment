package io.develab.parkingapp.availabilityservice.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SVY21Coordinate {
    private double easting;
    private double northing;
}
