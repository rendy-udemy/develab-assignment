package io.develab.parkingapp.availabilityservice.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LatLonCoordinate {
    private double latitude;
    private double longitude;
}
