package io.develab.parkingapp.availabilityservice.exception.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serial;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = 672505188305602313L;

    @Builder.Default
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime timestamp = OffsetDateTime.now();

    private ErrorStatus status;

    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String debugMessage;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<SubError> subErrors;

    public Map<String, Object> toAttributeMap() {
        return Map.of(
                "timestamp", this.timestamp,
                "status", this.status,
                "message", this.message,
                "debugMessage", this.debugMessage,
                "subErrors", this.subErrors
        );
    }

    public static class ErrorResponseBuilder {
        private OffsetDateTime timestamp;
        private ErrorStatus status;

        public ErrorResponseBuilder status(String code, String description, HttpStatus httpStatus) {
            this.status = ErrorStatus.builder()
                    .code(code)
                    .description(description)
                    .httpStatus(httpStatus)
                    .build();
            return this;
        }

        public ErrorResponseBuilder status(HttpStatus httpStatus) {
            this.status = ErrorStatus.builder()
                    .httpStatus(httpStatus)
                    .build();
            return this;
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ErrorStatus {
        private String code;
        private String description;
        @JsonIgnore
        private HttpStatus httpStatus;

        public String getCode() {
            if (code == null) {
                code = String.valueOf(httpStatus.value());
            }
            return code;
        }

        public String getDescription() {
            if (description == null) {
                description = httpStatus.getReasonPhrase();
            }
            return description;
        }
    }
}
