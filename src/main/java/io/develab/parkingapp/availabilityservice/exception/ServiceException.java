package io.develab.parkingapp.availabilityservice.exception;

import lombok.Getter;

@Getter
public class ServiceException extends RuntimeException {
    private ErrorCode errorCode;
    private String debugMessage;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, String debugMessage) {
        super(message);
        this.debugMessage = debugMessage;
    }

    public ServiceException(String message, Throwable cause, String debugMessage) {
        super(message, cause);
        this.debugMessage = debugMessage;
    }

    public ServiceException(Throwable cause, String debugMessage) {
        super(cause);
        this.debugMessage = debugMessage;
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String debugMessage) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.debugMessage = debugMessage;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ServiceException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ServiceException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public ServiceException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ServiceException(Throwable cause, ErrorCode errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ErrorCode errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public ServiceException(ErrorCode errorCode, String debugMessage) {
        this.errorCode = errorCode;
        this.debugMessage = debugMessage;
    }

    public ServiceException(String message, ErrorCode errorCode, String debugMessage) {
        super(message);
        this.errorCode = errorCode;
        this.debugMessage = debugMessage;
    }

    public ServiceException(String message, Throwable cause, ErrorCode errorCode, String debugMessage) {
        super(message, cause);
        this.errorCode = errorCode;
        this.debugMessage = debugMessage;
    }

    public ServiceException(Throwable cause, ErrorCode errorCode, String debugMessage) {
        super(cause);
        this.errorCode = errorCode;
        this.debugMessage = debugMessage;
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ErrorCode errorCode,
                            String debugMessage) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
        this.debugMessage = debugMessage;
    }

}
