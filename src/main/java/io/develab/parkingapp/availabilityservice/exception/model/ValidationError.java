package io.develab.parkingapp.availabilityservice.exception.model;

import java.io.Serial;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ValidationError extends SubError implements Serializable {
    @Serial
    private static final long serialVersionUID = 4050230412492309678L;

    private String object;
    private String field;
    private Object rejectedValue;
}
