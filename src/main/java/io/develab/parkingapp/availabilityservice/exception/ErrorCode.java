package io.develab.parkingapp.availabilityservice.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ErrorCode {
    ERR_001("ERR_001", "Car Park not found", HttpStatus.NOT_FOUND),
    ;

    private final String code;
    private final String description;
    private final HttpStatus httpStatus;

    ErrorCode(String code, String description, HttpStatus httpStatus) {
        this.code = code;
        this.description = description;
        this.httpStatus = httpStatus;
    }

    public ServiceException exception() {
        return new ServiceException(this.description, this);
    }

    public ServiceException exception(String debugMessage) {
        return new ServiceException(this.description, this, debugMessage);
    }
}
