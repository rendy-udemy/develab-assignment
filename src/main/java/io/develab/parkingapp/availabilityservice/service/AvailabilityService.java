package io.develab.parkingapp.availabilityservice.service;

import io.develab.parkingapp.availabilityservice.model.entity.Availability;
import io.develab.parkingapp.availabilityservice.model.mapper.AvailabilityMapper;
import io.develab.parkingapp.availabilityservice.repository.AvailabilityRepository;
import io.develab.parkingapp.availabilityservice.webclient.CarParkAvailabilityWebClient;
import io.develab.parkingapp.availabilityservice.webclient.model.CarParkAvailabilityResponse;
import io.develab.parkingapp.availabilityservice.webclient.model.CarParkData;
import io.develab.parkingapp.availabilityservice.webclient.model.CarParkInfo;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.spring.annotations.Recurring;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class AvailabilityService {
    private final AvailabilityRepository availabilityRepository;
    private final AvailabilityMapper availabilityMapper;

    private final CarParkAvailabilityWebClient availabilityWebClient;

    @Recurring(id = "check-availability-job", interval = "PT5M")
    @Job(name = "Check Availability Job")
    public void checkAvailabilityJob() {
        CarParkAvailabilityResponse response = availabilityWebClient.getAvailability().block();

        List<CarParkData> dataList = response.getItems().get(0).getCarparkData();

        List<Availability> entities = new ArrayList<>();

        for (CarParkData data : dataList) {
            CarParkInfo info = data.getCarparkInfo().get(0);
            Availability entity = Availability.builder()
                    .carParkNo(data.getCarparkNumber())
                    .lotType(info.getLotType())
                    .updateTimestamp(data.getUpdateDatetime().atOffset(ZoneOffset.UTC))
                    .lotsAvailable(info.getLots_available())
                    .totalLots(info.getTotalLots())
                    .createdBy("SYSTEM")
                    .updatedBy("SYSTEM")
                    .build();

            entities.add(entity);
        }

        for (Availability entity : entities) {
            Availability availability = availabilityRepository.findByCarParkNo(entity.getCarParkNo()).orElse(null);

            if (availability != null) {
                availability.setLotsAvailable(entity.getLotsAvailable());
                availability.setUpdateTimestamp(entity.getUpdateTimestamp());
                availabilityRepository.save(availability);
                continue;
            }

            availabilityRepository.save(entity);
        }
    }
}
