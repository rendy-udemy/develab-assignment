package io.develab.parkingapp.availabilityservice.service;

import io.develab.parkingapp.availabilityservice.exception.ErrorCode;
import io.develab.parkingapp.availabilityservice.model.PaginationResponse;
import io.develab.parkingapp.availabilityservice.model.dto.AvailabilityDto;
import io.develab.parkingapp.availabilityservice.model.dto.CarParkDto;
import io.develab.parkingapp.availabilityservice.model.dto.request.CarParkCriteria;
import io.develab.parkingapp.availabilityservice.model.dto.request.NearestCarParkCriteria;
import io.develab.parkingapp.availabilityservice.model.entity.CarPark;
import io.develab.parkingapp.availabilityservice.model.entity.projection.NearestCarPark;
import io.develab.parkingapp.availabilityservice.model.mapper.AvailabilityMapper;
import io.develab.parkingapp.availabilityservice.model.mapper.CarParkMapper;
import io.develab.parkingapp.availabilityservice.repository.CarParkRepository;
import java.util.Collections;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class CarParkService {
    private final CarParkRepository carParkRepository;
    private final CarParkMapper carParkMapper;

    private final AvailabilityMapper availabilityMapper;

    public CarParkDto getCarParkById(long id) {
        CarPark entity = carParkRepository.findById(id)
                .orElseThrow(ErrorCode.ERR_001::exception);

        return carParkMapper.toDto(entity);
    }

    public CarParkDto getCarPark(String carParkNo) {
        CarPark entity = carParkRepository.findByCarParkNoIgnoreCase(carParkNo)
                .orElseThrow(ErrorCode.ERR_001::exception);

        return carParkMapper.toDto(entity);
    }

    public PaginationResponse<CarParkDto> getCarParks(CarParkCriteria criteria) {
        PageRequest pageRequest = PageRequest.of(criteria.getCurrentPage() - 1, criteria.getPageSize(), criteria.getSortDirection(),
                criteria.getSortAttribute());
        Page<CarPark> carParks = new PageImpl<>(Collections.emptyList(), pageRequest, 0L);

        if (criteria.getCarParkType() == null && criteria.getParkingSystemType() == null) {
            carParks = carParkRepository.findAll(pageRequest);
        }

        if (criteria.getCarParkType() == null && criteria.getParkingSystemType() != null) {
            carParks = carParkRepository.findAllByParkingSystemType(criteria.getParkingSystemType(), pageRequest);
        }

        if (criteria.getCarParkType() != null && criteria.getParkingSystemType() == null) {
            carParks =
                    carParkRepository.findAllByCarParkType(criteria.getCarParkType(), pageRequest);
        }

        if (criteria.getCarParkType() != null && criteria.getParkingSystemType() != null) {
            carParks =
                    carParkRepository.findAllByCarParkTypeAndParkingSystemType(criteria.getCarParkType(), criteria.getParkingSystemType(),
                            pageRequest);
        }

        return PaginationResponse.fromPage(carParkMapper.toDto(carParks));
    }

    public PaginationResponse<AvailabilityDto> getNearestCarParks(NearestCarParkCriteria criteria) {
        PageRequest pageRequest =
                PageRequest.of(criteria.getCurrentPage() - 1, criteria.getPageSize());

        Page<NearestCarPark> nearestCarParks =
                carParkRepository.findAllNearestCarParks(criteria.getLatitude(), criteria.getLatitude(), pageRequest);

        return PaginationResponse.fromPage(availabilityMapper.toDto(nearestCarParks));
    }
}
