package io.develab.parkingapp.availabilityservice.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.develab.parkingapp.availabilityservice.exception.model.ErrorResponse;
import io.develab.parkingapp.availabilityservice.model.PaginationResponse;
import io.develab.parkingapp.availabilityservice.model.dto.AvailabilityDto;
import io.develab.parkingapp.availabilityservice.model.dto.CarParkDto;
import io.develab.parkingapp.availabilityservice.model.dto.request.CarParkCriteria;
import io.develab.parkingapp.availabilityservice.model.dto.request.NearestCarParkCriteria;
import io.develab.parkingapp.availabilityservice.service.CarParkService;
import io.develab.parkingapp.availabilityservice.util.CoordinateConverter;
import io.develab.parkingapp.availabilityservice.util.LatLonCoordinate;
import io.develab.parkingapp.availabilityservice.util.SVY21Coordinate;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("v1/carpark")
public class CarParkController {
    private final CarParkService carParkService;

    @Operation(summary = "Get Car Park Record By ID", description = "Returns Car Park record by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = CarParkDto.class))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @GetMapping(value = "{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CarParkDto> getCarPark(@PathVariable("id") String id) {
        return ResponseEntity.ok(carParkService.getCarPark(id));
    }

    @Operation(summary = "Get Car Park Records by filter criteria", description = "Returns Car Park records by its filter criteria")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = CarParkDto.class))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationResponse<CarParkDto>> getCarParks(@Valid @ParameterObject CarParkCriteria criteria) {
        return ResponseEntity.ok(carParkService.getCarParks(criteria));
    }

    @Operation(summary = "Get Nearest Car Park Records By filter criteria", description = "Returns Nearest Car Park records by its filter criteria")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = AvailabilityDto.class))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @GetMapping(value = "nearest", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationResponse<AvailabilityDto>> getNearestCarParks(@Valid @ParameterObject NearestCarParkCriteria criteria) {
        return ResponseEntity.ok(carParkService.getNearestCarParks(criteria));
    }
}
