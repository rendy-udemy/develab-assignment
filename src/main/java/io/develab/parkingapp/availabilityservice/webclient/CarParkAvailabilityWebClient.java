package io.develab.parkingapp.availabilityservice.webclient;

import io.develab.parkingapp.availabilityservice.webclient.model.CarParkAvailabilityResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Component
@Slf4j
public class CarParkAvailabilityWebClient {
    private final WebClient webClient;

    public Mono<CarParkAvailabilityResponse> getAvailability() {
        WebClient.UriSpec<WebClient.RequestBodySpec> uriSpec = webClient.method(HttpMethod.GET);
        WebClient.RequestBodySpec requestBodySpec =
                uriSpec.uri(uriBuilder -> uriBuilder.pathSegment("v1", "transport", "carpark-availability").build());

        return requestBodySpec.exchangeToMono(clientResponse -> {
            if (clientResponse.statusCode().is2xxSuccessful()) {
                return clientResponse.bodyToMono(CarParkAvailabilityResponse.class);
            } else {
                return clientResponse.createException()
                        .flatMap(Mono::error);
            }
        });
    }
}
